'use strict';
require('dotenv/config');
const cors = require('cors');
const https = require('https');
const fs = require('fs');
const path = require('path');
const express = require('express');
const errorhandler = require('errorhandler');
const notifier = require('node-notifier')
const bodyParser = require('body-parser');
const helmet = require('helmet');
const app = express();

const options = {
    key: fs.readFileSync(path.join(__dirname, 'ssl', 'key.pem'), 'utf8'),
    cert: fs.readFileSync(path.join(__dirname, 'ssl', 'cert.pem'), 'utf8'),
    passphrase: process.env.PASS_CERTIFICATE
};

function errorNotification(err, str, req) { notifier.notify({ title: 'Error in ' + req.method + ' ' + req.url, message: str }); }
if(process.env.NODE_ENV === 'development') { app.use(errorhandler({ log: errorNotification })); }
app.use(cors());
app.use(helmet());
app.use(bodyParser.json({limit: '100mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));
app.set("twig options", { allow_async: true, strict_variables: false });

// ROTAS
require('./routes/common-routes')(app);
require('./routes/usuario-routes')(app);
// END ROTAS

https.createServer(options, app).listen(process.env.PORT, function(err) {
    console.log('Listening HTTPS in Port: ' + process.env.PORT);
});

exports.app = app;