'use strict';
require('dotenv/config');
const jwt = require('jsonwebtoken');

// Middleware de TOKENIZAÇÃO de acesso baseado no HEADER: Authorization Bearer.
module.exports = async (req, res, next) => {
    const { authorization } = req.headers;
    if ( !authorization ) return res.status(401).json({ error: 'Sem token' });
    const parts = authorization.split(' ');
    if ( !parts.length === 2 ) return res.status(401).json({ error: 'Erro de token' });
    const [ scheme, token ] = parts;
    if ( !/^Bearer$/i.test(scheme) ) return res.status(401).json({ error: 'Token mal formado' });
    await jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
        if(err) return res.status(401).json({ error: 'Token inválido' });
        if(decoded && decoded.user && decoded.user.id) res.locals.userId = decoded.user.id;
        return next();
    });
};