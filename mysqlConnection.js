'use strict';
require('dotenv/config');
const mysql = require('mysql2/promise');
const bluebird = require('bluebird');
exports.getConnectionDB = async function () {
    try {
        const connection = await mysql.createConnection({
            host: process.env.HOST_DB,
            user: process.env.USER_DB,
            password: process.env.PASSWORD_DB,
            database: process.env.DATABASE_DB,
            Promise: bluebird
        });
        return connection;
    } catch(e) {
        console.error('Não foi possível conectar com o Banco de Dados.', e);
        return null;
    }
}