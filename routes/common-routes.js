'use strict';
require('dotenv/config');
const express = require('express');
const connection = require('../mysqlConnection');
const transport = require('../nodemailerConfig');
const router = express.Router();
const jwt = require('jsonwebtoken');
const Twig = require('twig');
const nodemailer = require('nodemailer');

router.post('/login', async (req, res) => {
    let { nome, senha } = req.body;
    if ( !nome ) return res.status(400).json({ msg: "É necessário ter Nome" });
    if ( !senha ) return res.status(400).json({ msg: "É necessário ter Senha" });
    const con = await connection.getConnectionDB();
    const [rows] = await con.query('SELECT * FROM usuario WHERE nome = ? AND senha = ?;', [nome, senha]);
    con.end();
    if(rows.length == 0) { return res.status(500).json({ msg: "Nenhum usuário encontrado, o nome ou a senha estão errados." }); }
    const token = await jwt.sign({ user: rows[0] }, process.env.SECRET_KEY);
    return res.status(200).json(token);
});

router.post('/send/email', async (req, res) => {
    try {
        await Twig.renderFile('./emails/recuperar-viajante.twig', { nome: 'SEU NOME' }, (err, pageRender) => {
            if ( err ) throw ('Erro ao renderizar.');
            nodemailer.createTestAccount( async (err, account) => {
                if ( err ) throw ('Erro ao criar envio de email.');
                let mailOptions = {
                    from: '"NOME QUALQUER" <no-replay@dominio.com.br>',
                    to: 'email@otherdominio.com.br',
                    subject: 'Assunto do email',
                    html: pageRender
                };
                let transp = await transport.getTransport(nodemailer);
                transp.sendMail(mailOptions, (error, info) => {
                    if( error ) throw ('Erro ao enviar email.');
                    return res.status(200).json({ msg: 'Email enviado.' });
                });
            });
        });
    } catch (error) {
        return res.status(500).json({ msg: error });
    }
});

module.exports = app => app.use('/', router);