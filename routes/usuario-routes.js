'use strict';
require('dotenv/config');
const express = require('express');
const connection = require('../mysqlConnection');
const authMiddleware = require('../auth');
const router = express.Router();

/*
    Rotas com o authMiddleware só seram acessadas usando o TOKEN gerado pela rota https://localhost:6002/login
    usando esse TOKEN no Header da requisição de cada rota como:
    Authorization: Bearer TOKEN
    Ex.: Authorization: Bearer eyJ1c2VyIjp7ImlkIjoxLCJ1c3Vhcm...
*/

// Pegar todos da tabela usuario
router.get('/', authMiddleware, async (req, res) => {
    const con = await connection.getConnectionDB();
    try {
        const [rows] = await con.query('SELECT nome FROM usuario;');
        if(rows.length == 0) throw ("Nenhum usuário encontrado!");
        con.end();
        return res.status(200).json(rows);
    } catch (error) {
        con.end();
        return res.status(500).json({ msg: error });
    }
});

// Pegar um da tabela usuario
router.get('/:id', authMiddleware, async (req, res) => {
    const con = await connection.getConnectionDB();
    const { id } = req.params;
    try {
        if ( !id ) throw ("É necessário ter ID");
        const [rows] = await con.query('SELECT nome FROM usuario WHERE id = ?;', [id]);
        if(rows.length == 0) throw ("Usuário não encontrado!");
        con.end();
        return res.status(200).json(rows[0]);
    } catch (error) {
        con.end();
        return res.status(500).json({ msg: error });
    }
});

// Inserir na tabela usuario
router.post('/', authMiddleware, async (req, res) => {
    const con = await connection.getConnectionDB();
    const { nome, senha } = req.body;
    try {
        if ( !nome ) throw ("É necessário ter um Nome");
        if ( !senha ) throw ("É necessário ter uma Senha");
        await con.query('INSERT INTO usuario (nome, senha) VALUES (?, ?);', [nome, senha]);
        con.end();
        return res.status(200).json({ msg: "Usuário inserido!" });
    } catch (error) {
        con.end();
        return res.status(500).json({ msg: error });
    }
});

// Atualizar nome de um usuario baseado no id
router.put('/:id', authMiddleware, async (req, res) => {
    const con = await connection.getConnectionDB();
    const { id } = req.params;
    const { nome } = req.body;
    try {
        if ( !id ) throw ("É necessário ter ID");
        if ( !nome ) throw ("É necessário ter um Nome");
        await con.query('UPDATE usuario SET nome = ? WHERE id = ?;', [nome, id]);
        con.end();
        return res.status(200).json({ msg: "Usuário atualizado!" });
    } catch (error) {
        con.end();
        return res.status(500).json({ msg: error });
    }
});

// Deletar um usuario baseado no id
router.delete('/:id', authMiddleware, async (req, res) => {
    const con = await connection.getConnectionDB();
    const { id } = req.params;
    try {
        if ( !id ) throw ("É necessário ter ID");
        await con.query('DELETE FROM usuario WHERE id = ?;', [id]);
        con.end();
        return res.status(200).json({ msg: "Usuário deletado!" });
    } catch (error) {
        con.end();
        return res.status(500).json({ msg: error });
    }
});

// localhost:PORT/usuario
module.exports = app => app.use('/usuario', router);