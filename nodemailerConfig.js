require('dotenv/config');

exports.getTransport = async function(nodemailer) {
    let transp = await nodemailer.createTransport({
        host: process.env.SMTP_HOST,
        port: 587,
        secure: false,
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASS
        }
    });
    return transp;
}