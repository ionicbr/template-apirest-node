# Template APIRest em Node com JWT e MySQL
> Template básico para auxiliar o início de criação de uma APIRest em NodeJS com JWT, HTTPS, NodeMailer e TWIG (Que eu uso para gerar .html dinamico para enviar email em html com o nodemailer por meio de SMTP)


## Como usar em desenvolvimento

Ao terminar de clonar o projeto para seu computador primeiramente gere o <b>certificado auto assinado</b> com a instrução abaixo.<br/>
Utilize os comando abaixo linha a linha seguindo suas instruções, será criado os arquivos <b>key.pem</b> e <b>csr.pem</b>, que voce colocará na pasta <b>SSL</b> deste projeto.<br/>
```sh
openssl genrsa -out key.pem
openssl req -new -key key.pem -out csr.pem
openssl x509 -req -days 9999 -in csr.pem -signkey key.pem -out cert.pem
rm csr.pem
```
`lembrando de gravar a senha utilizada na geração desses arquivos em algum lugar seguro`

Após gerar os arquivos e colocar eles dentro da pasta <b>SSL</b> deste projeto copie o arquivo <b>.env.example</b> que tem na pasta do projeto e renomeio a copia para <b>.env</b><br/>
Dentro dele você encontrará o seguinte conteudo:

```sh
# Porta a ser utilizada pelo node.
PORT = 6001

# NodeServe em modo development, coloque: production no arquivo do servidor.
NODE_ENV = development

# IP da onde está o banco de dados.
HOST_DB = localhost

# Usuario de acesso ao banco de dados.
USER_DB = username

# Senha de acesso ao banco de dados.
PASSWORD_DB = dbpass

# Database a ser usado.
DATABASE_DB = dbname

# Senha para codificar o JWT.
SECRET_KEY = mySuperSecretKey

# Senha do certificado.
PASS_CERTIFICATE = eueu

# HOST DO SEU SMTP
SMTP_HOST = smtp.dominio.com

# Usuário do SMTP
SMTP_USER = usuario

# Senha do SMTP
SMTP_PASS = senha
```

Preencha conforme a sua configuração, lembre-se no seu servidor de produção você deverá criar esse arquivo <b>.env</b> pois por questões de segurança não subimos o <b>.env</b> configurado para o git.


### Criar um novo arquivo de rotas
Basta criar um arquivo <b>.js</b> na pasta <b>routes</b> e no arquivo <b>app.js</b> você verá o bloco abaixo, é só adicionar o novo arquivo como segue os outros.
```js
// ROTAS
require('./routes/common-routes')(app);
require('./routes/usuario-routes')(app);
// END ROTAS
```

O arquivo <b>usuario-routes.js</b> nada mais é do que um exemplo de como pode ser de uma forma organizada e detalhada os seus arquivos de rotas, note que nesse mesmo arquivo no final da página dele tem o bloco de código:
```js
module.exports = app => app.use('/usuario', router);
```
Ou seja para acessar esse arquivo voce utilizará: <b>https://localhost:6001/usuario</b>


### Exemplo de Login e gerando JWT
no arquivo <b>common-routes.js</b> dentro da pasta <b>routes</b> voce encontrará o código a seguir:
```js
router.post('/login', async (req, res) => {
    let { nome, senha } = req.body;
    if ( !nome ) return res.status(400).json({ msg: "É necessário ter Nome" });
    if ( !senha ) return res.status(400).json({ msg: "É necessário ter Senha" });
    const con = await connection.getConnectionDB();
    const [rows] = await con.execute('SELECT * FROM usuario WHERE nome = ? AND senha = ?;', [usuario, senha]);
    con.end();
    if(rows.length == 0) { return res.status(500).json({ msg: "Nenhum usuário encontrado, o nome ou a senha estão errados." }); }
    const token = await jwt.sign({ user: rows[0] }, process.env.SECRET_KEY);
    return res.status(200).json(token);
});
```

Esse bloco de código acima faz uma busca no banco de dados na tabela usuario pra saber se tem algum usuario com a senha, caso tenha, ele cria um <b>TOKEN JWT</b> em cima do retorno da busca no banco e retorna para o usuário, esse <b>Token</b> você salva ele como desejar, seja ele por <b>localStorage</b> ou algo do seu gosto.

### Requerimentos

* npm 6.4.1
* node 10.1.0
* bluebird 3.5.2
* body-parser 1.18.2
* cors 2.8.4
* dotenv 5.0.1
* errorhandler 1.5.0
* express 4.16.3
* helmet 3.12.0
* jsonwebtoken 8.2.1
* mysql2 1.6.1
* node-notifier 5.2.1
* nodemailer 4.6.8
* twig 1.12.0


### Instalar
Basta executar o comando abaixo
```sh
npm install --save
```

### Rodar
```sh
node start
```

## Autor

Lennon Milicic Costa
* [IONICBR](https://www.ionicbr.com.br/)
* [GITLAB](https://gitlab.com/lennon)